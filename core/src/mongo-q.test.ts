import {main} from './mongo-q';
describe("using mongo", () => {
    it("call main" ,async () => {
        const val = await main();
        expect(val).toEqual(3);
    });
});