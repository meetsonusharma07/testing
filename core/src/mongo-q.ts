import {MongoClient} from 'mongodb';
const uri = "mongodb://mongo/mydb?authSource=admin"
const client = new MongoClient(uri, { useNewUrlParser: true });

async function listDatabases(){
    const val  = await client.db().admin().listDatabases();
    return val.databases.length;
}

export async function main(){
    await client.connect();
    const val = await listDatabases();
    await client.close();
    return val;
}
